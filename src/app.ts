import "../styles.css";
import { StoryInput } from "./components/story-input";
import { StoryList } from "./components/story-list";

new StoryList("backlog");
new StoryList("ready");
new StoryList("inprogress");
new StoryList("complete");
new StoryInput();
