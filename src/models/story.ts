export enum StoryStatus {
  Backlog,
  Ready,
  InProgress,
  Complete,
}

export class Story {
  constructor(
    public readonly id: string,
    public readonly title: string,
    public readonly estimate: number,
    public status: StoryStatus
  ) {}
}
