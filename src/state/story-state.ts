import { Story, StoryStatus } from "../models/story";

type Listener<T> = (items: T[]) => void;

class State<T> {
  protected listeners: Listener<T>[] = [];

  addListener(listenerFn: Listener<T>) {
    this.listeners.push(listenerFn);
  }
}

export class StoryState extends State<Story> {
  private stories: Story[] = [];
  private static instance: StoryState;

  private constructor() {
    super();
  }

  static getInstance() {
    if (this.instance) {
      return this.instance;
    }
    this.instance = new StoryState();
    return this.instance;
  }

  addStory(title: string, estimate: number) {
    const newStory = new Story(
      Math.random().toString(),
      title,
      estimate,
      StoryStatus.Backlog
    );
    this.stories.push(newStory);

    this.updateListeners();
  }

  moveStory(storyId: string, newStatus: StoryStatus) {
    const story = this.stories.find((st) => st.id === storyId);
    if (story && story.status !== newStatus) {
        story.status = newStatus;
      this.updateListeners();
    }
  }

  private updateListeners() {
    for (const listenerFn of this.listeners) {
      listenerFn(this.stories.slice());
    }
  }
}

export const storyState = StoryState.getInstance();
