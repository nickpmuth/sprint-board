import { Draggable } from "../models/drag-drop";
import { Story } from "../models/story";
import { Component } from "./base-component";
import { autobind } from "../decorators/autobind";

export class StoryItem extends Component<HTMLUListElement, HTMLLIElement>
  implements Draggable {
  private story: Story;

  get persons() {
    if (this.story.estimate === 1) {
      return "1 person";
    }
    return `${this.story.estimate} persons`;
  }

  constructor(hostId: string, story: Story) {
    super("single-story", hostId, false, story.id);
    this.story = story;

    this.configure();
    this.renderContent();
  }

  @autobind
  dragStartHandler(event: DragEvent) {
    event.dataTransfer!.setData("text/plain", this.story.id);
    event.dataTransfer!.effectAllowed = "move";
  }

  dragEndHandler(_: DragEvent) {
    console.log("DragEnd");
  }

  configure() {
    this.element.addEventListener("dragstart", this.dragStartHandler);
    this.element.addEventListener("dragend", this.dragEndHandler);
  }

  renderContent() {
    this.element.querySelector("h2")!.textContent = this.story.title;
    //this.element.querySelector("h3")!.textContent = this.persons + " assigned";
    this.element.querySelector("p")!.textContent = this.story.estimate.toString();
  }
}
