import { DragTarget } from "../models/drag-drop";
import { Story, StoryStatus } from "../models/story";
import { Component } from "./base-component";
import { autobind } from "../decorators/autobind";
import { storyState } from "../state/story-state";
import { StoryItem } from "./story-item";

export class StoryList extends Component<HTMLDivElement, HTMLElement>
  implements DragTarget {
  assignedStories: Story[];

  constructor(private type: "backlog" | "ready" | "inprogress" | "complete") {
    super("story-list", "app", false, `${type}-stories`);
    this.assignedStories = [];

    this.configure();
    this.renderContent();
  }

  get friendlyStatusType(): string {
    let friendlyText = "";
    if (this.type === "inprogress") {
      friendlyText = "In Progress";
    } else {
      friendlyText = this.type;
    }
    return friendlyText;
  }

  @autobind
  dragOverHandler(event: DragEvent) {
    if (event.dataTransfer && event.dataTransfer.types[0] === "text/plain") {
      event.preventDefault();
      const sectionEl = document.querySelector(`#${this.type}-stories`)!;
      sectionEl.classList.add("droppable");
    }
  }

  @autobind
  dropHandler(event: DragEvent) {
    const stId = event.dataTransfer!.getData("text/plain");
    storyState.moveStory(stId, this.getStatusMap(this.type));
    this.removeDroppableClass();
  }

  @autobind
  dragLeaveHandler(_: DragEvent) {
    this.removeDroppableClass();
  }

  configure() {
    this.element.addEventListener("dragover", this.dragOverHandler);
    this.element.addEventListener("dragleave", this.dragLeaveHandler);
    this.element.addEventListener("drop", this.dropHandler);

    storyState.addListener((stories: Story[]) => {
      const relevantStories = stories.filter((st) => {
        switch (this.type) {
          case "ready":
            return st.status === StoryStatus.Ready;
          case "inprogress":
            return st.status === StoryStatus.InProgress;
          case "complete":
            return st.status === StoryStatus.Complete;
          default:
            return st.status === StoryStatus.Backlog;
        }
      });
      this.assignedStories = relevantStories;
      this.renderStories();
    });
  }

  renderContent() {
    const listId = `${this.type}-stories-list`;
    this.element.querySelector("ul")!.id = listId;
    this.element.querySelector(
      "h2"
    )!.textContent = this.friendlyStatusType.toUpperCase();
  }

  private removeDroppableClass() {
    const sectionEl = document.querySelector(`#${this.type}-stories`)!;
    sectionEl.classList.remove("droppable");
  }

  private renderStories() {
    const listEl = document.getElementById(
      `${this.type}-stories-list`
    )! as HTMLUListElement;
    listEl.innerHTML = "";
    const labelEl = this.element.querySelector("span")!;
    labelEl.textContent = "";
    for (const stItem of this.assignedStories) {
      new StoryItem(this.element.querySelector("ul")!.id, stItem);
    }
    if (this.assignedStories.length) {
      labelEl.textContent = `(${this.assignedStories.length.toString()})`;
    }
  }

  private getStatusMap(type: string): StoryStatus {
    let stStatus = StoryStatus.Backlog;
    switch (type) {
      case "backlog":
        stStatus = StoryStatus.Backlog;
        break;
      case "ready":
        stStatus = StoryStatus.Ready;
        break;
      case "inprogress":
        stStatus = StoryStatus.InProgress;
        break;
      case "complete":
        stStatus = StoryStatus.Complete;
        break;
    }
    return stStatus;
  }
}
