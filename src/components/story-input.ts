import { Component } from "./base-component";
import * as Validation from "../util/validation";
import { autobind  } from "../decorators/autobind";
import { storyState } from "../state/story-state";

export class StoryInput extends Component<HTMLDivElement, HTMLFormElement> {
  titleInputElement: HTMLInputElement;
  estimateInputElement: HTMLInputElement;

  constructor() {
    super("story-input", "backlog-stories", false, "user-input");

    this.titleInputElement = this.element.querySelector(
      "#title"
    ) as HTMLInputElement;
    this.estimateInputElement = this.element.querySelector(
      "#estimate"
    ) as HTMLInputElement;

    this.configure();
  }

  configure() {
    //this.element.addEventListener('submit', this.submitHandler.bind(this));
    this.element.addEventListener("submit", this.submitHandler);
  }

  renderContent() {}

  private gatherUserInput(): [string, number] | void {
    const enteredTitle = this.titleInputElement.value;
    const enteredEstimate = this.estimateInputElement.value;

    const titleValidatable: Validation.Validatable = {
      value: enteredTitle,
      required: true,
    };

    const estimateValidatable: Validation.Validatable = {
      value: +enteredEstimate,
      required: true,
      min: 1,
      max: 5,
    };

    if (
      !Validation.validate(titleValidatable) ||
      !Validation.validate(estimateValidatable)
    ) {
      alert("Invalid input");
      return;
    }

    return [enteredTitle, +enteredEstimate];
  }

  private clearInputs() {
    this.titleInputElement.value = "";
    this.estimateInputElement.value = "";
  }

  @autobind
  private submitHandler(event: Event) {
    event.preventDefault();
    const userInput = this.gatherUserInput();
    if (Array.isArray(userInput)) {
      const [title, estimate] = userInput;
      console.log(title, estimate);
      storyState.addStory(title, estimate);
      this.clearInputs();
    }
  }
}
